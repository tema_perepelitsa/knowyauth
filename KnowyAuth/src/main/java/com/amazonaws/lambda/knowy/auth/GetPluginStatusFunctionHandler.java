package com.amazonaws.lambda.knowy.auth;

import com.amazonaws.lambda.knowy.auth.GetPluginStatusFunctionHandler.RequestClass;
import com.amazonaws.lambda.knowy.auth.GetPluginStatusFunctionHandler.ResponseClass;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.util.StringUtils;

public class GetPluginStatusFunctionHandler implements RequestHandler<RequestClass, ResponseClass> {
	static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
	static DynamoDB dynamoDB = new DynamoDB(client);
	private String USERS_TABLE_NAME = "KnowyUsers";
	private LambdaLogger logger = null;

	@Override
	public ResponseClass handleRequest(RequestClass input, Context context) {
		logger = context.getLogger();
		if (logger == null) {
			throw new IllegalArgumentException("No logger defined");
		}
		String id = input.getIdentityId();
		if (StringUtils.isNullOrEmpty(id)) {
			throw new IllegalArgumentException("No identity in context");
		}
		ResponseClass output = new ResponseClass();
		logger.log(String.format("Identity requested: %s", id));
		Table usersTable = dynamoDB.getTable(USERS_TABLE_NAME);
		Item user = usersTable.getItem("identityId", id);
		String openId = user.getString("openIdentityId");
		output.setPluginConnected(openId != null);
		if (output.isPluginConnected()) {
			Item fakeUser = usersTable.getItem("identityId", openId);
			if (fakeUser != null) {
				output.setLastDataSync(fakeUser.getString("lastPluginDataSent"));
			}
		}
		return output;
	}

	public class ResponseClass {
		private boolean pluginConnected;
		private String lastDataSync;

		public boolean isPluginConnected() {
			return pluginConnected;
		}

		public void setPluginConnected(boolean pluginConnected) {
			this.pluginConnected = pluginConnected;
		}

		public String getLastDataSync() {
			return lastDataSync;
		}

		public void setLastDataSync(String lastDataSync) {
			this.lastDataSync = lastDataSync;
		}
	}

	public static class RequestClass {
		private String identityId;

		public RequestClass() {
		}

		public RequestClass(String identityId) {
			this.identityId = identityId;
		}

		public String getIdentityId() {
			return identityId;
		}

		public void setIdentityId(String identityId) {
			this.identityId = identityId;
		}
	}
}