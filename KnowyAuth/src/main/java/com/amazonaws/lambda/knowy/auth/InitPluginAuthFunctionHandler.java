package com.amazonaws.lambda.knowy.auth;

import java.util.Random;

import org.apache.commons.codec.digest.DigestUtils;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.lambda.knowy.auth.InitPluginAuthFunctionHandler.RequestClass;
import com.amazonaws.services.cognitoidentity.AmazonCognitoIdentity;
import com.amazonaws.services.cognitoidentity.AmazonCognitoIdentityClientBuilder;
import com.amazonaws.services.cognitoidentity.model.IdentityDescription;
import com.amazonaws.services.cognitoidentity.model.ListIdentitiesRequest;
import com.amazonaws.services.cognitoidentity.model.ListIdentitiesResult;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.util.StringUtils;

public class InitPluginAuthFunctionHandler implements RequestHandler<RequestClass, String> {
	static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
	static DynamoDB dynamoDB = new DynamoDB(client);
	private String DYNAMODB_TABLE_NAME = "AuthToken";
	private LambdaLogger logger = null;

	@Override
	public String handleRequest(RequestClass input, Context context) {
		logger = context.getLogger();
		if (logger == null) {
			throw new IllegalArgumentException("No logger defined");
		}
		String identity = input.getIdentityId();
		if (StringUtils.isNullOrEmpty(identity)) {
			throw new IllegalArgumentException("No identity in context");
		}
		logger.log(String.format("Request from: %s", identity));
		int key = generateKey(identity);
		logger.log(String.format("Generated key: %s", key));
		putKey(identity, key);
		return String.format("%04d", key);
	}

	private void putKey(String identity, int key) {
		Table table = dynamoDB.getTable(DYNAMODB_TABLE_NAME);
		// if no identity - get random one from db __ DEV
		Item item = new Item().withPrimaryKey("Key", key).withString("IdentityId", identity).withNumber("ExpTime",
				System.currentTimeMillis() / 1000 + 300);
		table.putItem(item);
	}

	private int generateKey(String base) {
		int res = 0;
		int iter = 0;
		do {
			int salt = new Random().nextInt();
			res = Math.abs(DigestUtils.sha256Hex(base).hashCode() * salt % 10000);
			iter++;
		} while (!isKeyUnique(res) && iter < 10);
		if (iter == 10)
			throw new AmazonServiceException("Unable to create unique key within specified number of iterations");
		return res;
	}

	// DEV only
	private String getNotNullIdentity(String identity) {
		String res = identity;
		if (StringUtils.isNullOrEmpty(res)) {
			final AmazonCognitoIdentity aci = AmazonCognitoIdentityClientBuilder.defaultClient();
			ListIdentitiesRequest request = new ListIdentitiesRequest();
			request.setIdentityPoolId("us-east-1:29ef240d-3377-4ecf-b0ce-2e09a0d2ba93");
			request.setMaxResults(60);
			ListIdentitiesResult response = aci.listIdentities(request);
			int r = new Random().nextInt(response.getIdentities().size());
			IdentityDescription i = response.getIdentities().get(r);
			res = i.getIdentityId();
			logger.log(String.format("Identities num %s", response.getIdentities().size()));
			logger.log(String.format("Generated random %s", r));
			logger.log(String.format("Random identityId %s", res));
		}
		return res;
	}

	private boolean isKeyUnique(int key) {
		Table table = dynamoDB.getTable(DYNAMODB_TABLE_NAME);
		Item item = table.getItem("Key", key, "IdentityId", null);
		return item == null;
	}

	public static class RequestClass {
		private String identityId;

		public RequestClass() {
		}

		public RequestClass(String identityId) {
			this.identityId = identityId;
		}

		public String getIdentityId() {
			return identityId;
		}

		public void setIdentityId(String identityId) {
			this.identityId = identityId;
		}
	}
}