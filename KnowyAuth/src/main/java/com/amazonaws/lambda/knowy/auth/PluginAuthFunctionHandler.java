package com.amazonaws.lambda.knowy.auth;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.lambda.knowy.auth.PluginAuthFunctionHandler.RequestClass;
import com.amazonaws.lambda.knowy.auth.PluginAuthFunctionHandler.ResponseClass;
import com.amazonaws.services.cognitoidentity.AmazonCognitoIdentity;
import com.amazonaws.services.cognitoidentity.AmazonCognitoIdentityClientBuilder;
import com.amazonaws.services.cognitoidentity.model.GetCredentialsForIdentityRequest;
import com.amazonaws.services.cognitoidentity.model.GetCredentialsForIdentityResult;
import com.amazonaws.services.cognitoidentity.model.GetOpenIdTokenForDeveloperIdentityRequest;
import com.amazonaws.services.cognitoidentity.model.GetOpenIdTokenForDeveloperIdentityResult;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.util.StringUtils;

public class PluginAuthFunctionHandler implements RequestHandler<RequestClass, ResponseClass> {
	static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
	static DynamoDB dynamoDB = new DynamoDB(client);
	private String AUTH_TABLE_NAME = "AuthToken";
	private String USERS_TABLE_NAME = "KnowyUsers";
	private String IDENTITY_POOL_ID = "us-east-1:29ef240d-3377-4ecf-b0ce-2e09a0d2ba93";
	private String CUSTOM_IDENTITY_PROVIDER = "plugin.auth.knowy.com";
	private LambdaLogger logger = null;

	@Override
	public ResponseClass handleRequest(RequestClass input, Context context) {
		logger = context.getLogger();
		AmazonCognitoIdentity aci = AmazonCognitoIdentityClientBuilder.defaultClient();
		logger.log(String.format("Key requested: %s", input.getKey()));
		ResponseClass output = new ResponseClass();
		if (!StringUtils.isNullOrEmpty(input.getOpenId())) {
			logger.log("Refreshing token");
			GetCredentialsForIdentityRequest getCredsRequest = new GetCredentialsForIdentityRequest();
			Map<String, String> logins = new HashMap<>();
			logins.put(CUSTOM_IDENTITY_PROVIDER, input.getOpenId());
			getCredsRequest.setIdentityId(input.getOpenId());
			getCredsRequest.setLogins(logins);
			GetCredentialsForIdentityResult getCredsResult = aci.getCredentialsForIdentity(getCredsRequest);
			output.setToken(getCredsResult.getCredentials().getSessionToken());
			output.setIdentityId(getCredsResult.getIdentityId());
			output.setUserName("Temp test");
		} else {
			Table authTable = dynamoDB.getTable(AUTH_TABLE_NAME);
			Item identity = authTable.getItem("Key", input.getKey(), "IdentityId, ExpTime", null);
			long curTime = System.currentTimeMillis() / 1000;
			logger.log(String.format("Found identity: %s", identity));
			if (identity != null && (curTime < identity.getLong("ExpTime"))) {
				String identityId = identity.getString("IdentityId");
				Table usersTable = dynamoDB.getTable(USERS_TABLE_NAME);
				Item user = usersTable.getItem("identityId", identityId);

				Map<String, String> logins = new HashMap<>();
				logins.put(CUSTOM_IDENTITY_PROVIDER, identityId);
				GetOpenIdTokenForDeveloperIdentityRequest getOpenIdRequest = new GetOpenIdTokenForDeveloperIdentityRequest();
				//getOpenIdRequest.setIdentityId(identityId);
				getOpenIdRequest.setIdentityPoolId(IDENTITY_POOL_ID);
				getOpenIdRequest.setLogins(logins);
				getOpenIdRequest.setTokenDuration(60l); // constraint for max duration time - 24 hours
				GetOpenIdTokenForDeveloperIdentityResult openIdResult = aci
						.getOpenIdTokenForDeveloperIdentity(getOpenIdRequest);
				logger.log(String.format("Open id found: %s", openIdResult.getIdentityId()));
				logger.log(String.format("Open token found: %s", openIdResult.getToken()));
				output.setToken(openIdResult.getToken());
				UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey("identityId", identityId)
						.withUpdateExpression("set #na = :val1")
						.withNameMap(new NameMap().with("#na", "openIdentityId"))
						.withValueMap(new ValueMap().withString(":val1", openIdResult.getIdentityId()))
						.withReturnValues(ReturnValue.ALL_NEW);
				UpdateItemOutcome outcome = usersTable.updateItem(updateItemSpec);
				logger.log("Printing KnowyUser after adding new attribute...");
				logger.log(outcome.getItem().toJSONPretty());
				output.setIdentityId(openIdResult.getIdentityId());
				output.setUserName(String.format("%s %s",
						StringUtils.isNullOrEmpty(user.getString("name")) ? "Unknown" : user.getString("name"),
						StringUtils.isNullOrEmpty(user.getString("family_name")) ? "Unknown"
								: user.getString("family_name")));
			}
		}
		return output;
	}

	public static class RequestClass {
		private Integer key;
		private String openId;

		public RequestClass() {
		}

		public RequestClass(Integer key, String openId) {
			this.key = key;
			this.openId = openId;
		}

		public Integer getKey() {
			return key;
		}

		public void setKey(Integer key) {
			this.key = key;
		}

		public String getOpenId() {
			return openId;
		}

		public void setOpenId(String openId) {
			this.openId = openId;
		}
	}

	public class ResponseClass {
		private String userName;
		private String token;
		private String identityId;

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getToken() {
			return token;
		}

		public void setToken(String token) {
			this.token = token;
		}

		public String getIdentityId() {
			return identityId;
		}

		public void setIdentityId(String identityId) {
			this.identityId = identityId;
		}
	}
}